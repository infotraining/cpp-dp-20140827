#ifndef COFFEEHELL_HPP_
#define COFFEEHELL_HPP_

#include <iostream>
#include <string>
#include <memory>
#include <boost/utility/base_from_member.hpp>

class Coffee
{
protected:
	float price_;
	std::string description_;
public:
	Coffee(float price, const std::string& description) : price_(price), description_(description)
	{
	}

	virtual float get_total_price() const
	{
		return price_;
	}

	virtual std::string get_description() const
	{
		return description_;
	}

	virtual void prepare() = 0;


	virtual ~Coffee() {}
};

class Espresso : public Coffee
{
public:
	Espresso(float price = 4.0, const std::string& description = "Espresso")
		: Coffee(price, description)
	{
	}

	void prepare()
	{
		std::cout << "Making a perfect espresso: 7 g, 15 bar and 24 sec.\n";
	}
};

class Cappuccino : public Coffee
{
public:
	Cappuccino(float price = 6.0, const std::string& description = "Cappuccino")
		: Coffee(price, description)
	{
	}

	void prepare()
	{
		std::cout << "Making a perfect cappuccino.\n";
	}
};

class Latte : public Coffee
{
public:
	Latte(float price = 8.0, const std::string& description = "Latte")
		: Coffee(price, description)
	{
	}

	void prepare()
	{
		std::cout << "Making a perfect latte.\n";
	}
};

class CoffeeDecorator : public Coffee
{
    std::unique_ptr<Coffee> coffee_;
public:
    CoffeeDecorator(std::unique_ptr<Coffee> coffee, float price, const std::string& description)
        :  Coffee(price, description), coffee_(std::move(coffee))
    {}

    std::string get_description() const
    {
        return coffee_->get_description() + " + " + Coffee::get_description();
    }

    float get_total_price() const
    {
        return Coffee::get_total_price() + coffee_->get_total_price();
    }

    void prepare()
    {
        coffee_->prepare();
    }
};

class Whipped : public CoffeeDecorator
{
public:
    Whipped(std::unique_ptr<Coffee> coffee) : CoffeeDecorator(std::move(coffee), 2.5F, "Whipped cream")
    {}

    void prepare()
    {
        CoffeeDecorator::prepare();
        std::cout << "Adding whipped cream..." << std::endl;
    }
};

class Whisky : public CoffeeDecorator
{
public:
    Whisky(std::unique_ptr<Coffee> coffee) : CoffeeDecorator(std::move(coffee), 6.0F, "Whisky")
    {}

    void prepare()
    {
        CoffeeDecorator::prepare();
        std::cout << "Pouring a 50cl of whisky..." << std::endl;
    }
};

class ExtraEspresso : private boost::base_from_member<Espresso>, public CoffeeDecorator
{
public:
    ExtraEspresso(std::unique_ptr<Coffee> coffee)
        : CoffeeDecorator(std::move(coffee), member.get_total_price(), "Extra espresso")
    {}

    void prepare()
    {
        CoffeeDecorator::prepare();
        member.prepare();
    }
};


// TO DO: Dodatki: cena - Whipped: 2.5, Whiskey: 6.0, ExtraEspresso: 4.0

// TO DO: Utworzy� klas� CoffeeDecorator i klasy konkretnych dodatk�w.
//        Utworzy� espresso udekorowane dodatkami, obliczy� cen�, wy�wietli� opis i przygotowa� nap�j.

#endif /*COFFEEHELL_HPP_*/
