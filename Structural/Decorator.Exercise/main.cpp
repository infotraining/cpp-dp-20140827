#include "coffeehell.hpp"

class CoffeeBuilder
{
    std::unique_ptr<Coffee> coffee_;
public:
    template <typename T>
    CoffeeBuilder& create_base()
    {
        coffee_.reset(new T());
        return *this;
    }

    template <typename T>
    CoffeeBuilder& add()
    {
        coffee_ = std::unique_ptr<Coffee>(new T(std::move(coffee_)));

        return *this;
    }

    std::unique_ptr<Coffee> get_coffee()
    {
        return std::move(coffee_);
    }
};

int main()
{
//    std::unique_ptr<Coffee> cf(
//                std::unique_ptr<Coffee>(new Whipped(
//                    std::unique_ptr<Coffee>(new Whisky(
//                    std::unique_ptr<Coffee>(new Whisky(
//                        std::unique_ptr<Coffee>(new ExtraEspresso(
//                                                    std::unique_ptr<Coffee>(new Espresso()))))))))));

    CoffeeBuilder cb;

    cb.create_base<Espresso>();
    cb.add<ExtraEspresso>().add<Whisky>().add<Whisky>().add<Whipped>();

    std::unique_ptr<Coffee> cf = cb.get_coffee();

	std::cout << "Description: " << cf->get_description() << "; Price: " << cf->get_total_price() << std::endl;
	cf->prepare();
}
