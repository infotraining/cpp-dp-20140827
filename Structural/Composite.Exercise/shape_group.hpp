#ifndef SHAPE_GROUP_HPP_
#define SHAPE_GROUP_HPP_

#include "shape.hpp"
#include "clone_factory.hpp"
#include <memory>

namespace Drawing
{

    // TO DO: zaimplementowac kompozyt grupuj�cy kszta�ty geometryczne
    class ShapeGroup : public Shape
    {
        std::vector<std::shared_ptr<Shape>> shapes_;
    public:
        ShapeGroup()
        {
        }

        ShapeGroup(const ShapeGroup& source)
        {
            for(auto s : source.shapes_)
            {
                shapes_.emplace_back(s->clone());
            }
        }

        void draw() const
        {
            for(auto s : shapes_)
                s->draw();
        }

        void move(int dx, int dy)
        {
            for(auto s : shapes_)
                s->move(dx, dy);
        }

        ShapeGroup* clone() const
        {
            return new ShapeGroup(*this);
        }

        void read(std::istream &in)
        {
            int count;

            in >> count;

            std::string type_identifier;
            for(int i = 0; i < count; ++i)
            {
                in >> type_identifier;

                Shape* shp = ShapeFactory::instance().create(type_identifier);
                shp->read(in);

                shapes_.emplace_back(shp);
            }
        }

        void write(std::ostream &out)
        {
            out << "ShapeGroup " << shapes_.size() << std::endl;

            for(auto s : shapes_)
                s->write(out);
        }
    };

}

#endif /*SHAPE_GROUP_HPP_*/
