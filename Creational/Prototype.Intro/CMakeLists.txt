project(Prototype)
cmake_minimum_required(VERSION 2.8)
aux_source_directory(. SRC_LIST)
ADD_DEFINITIONS("-std=c++0x")
add_executable(${PROJECT_NAME} ${SRC_LIST})

