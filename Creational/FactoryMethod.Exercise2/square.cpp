#include "square.hpp"
#include "shape_factory.hpp"

namespace
{
    using namespace Drawing;

    bool is_drawing =
            ShapeFactory::instance()
                .register_creator("Square", ShapeCreator<Square>());
}
