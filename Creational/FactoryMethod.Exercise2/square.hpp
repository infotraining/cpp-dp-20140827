/*
 * square.hpp
 *
 *  Created on: 04-02-2013
 *      Author: Krystian
 */

#ifndef SQUARE_HPP_
#define SQUARE_HPP_

#include "shape.hpp"

// TODO: Doda� klase Square

namespace Drawing
{
    class Square : public ShapeBase
    {
        int side_;
    public:
        Square(int x = 0, int y = 0, int side = 0) : ShapeBase(x, y), side_(side)
        {}

        void set_size(int side)
        {
            side_ = side;
        }

        int side() const
        {
            return side_;
        }

        void draw() const
        {
            std::cout << "Drawing square at " << point() << " with side " << side_ << std::endl;
        }

        void read(std::istream &in)
        {
            Point pt;
            int size;

            in >> pt >> size;
            set_point(pt);
            set_size(size);
        }

        void write(std::ostream &out)
        {
            out << "Square " << point() << " " << side() << std::endl;
        }
    };
}

#endif /* SQUARE_HPP_ */
