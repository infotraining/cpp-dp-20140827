#include <iostream>
#include <vector>
#include <functional>
#include <algorithm>

using namespace std;

#define WINDOWS

class Widget {
public:
    virtual void draw() = 0;
    virtual ~Widget() {}
};

class WidgetFactory
{
public:
    virtual Widget* create_menu() const = 0;
    virtual Widget* create_button() const = 0;
    virtual ~WidgetFactory() {}
};

class MotifButton : public Widget {
public:
    void draw() {
        cout << "MotifButton\n";
    }
};

class MotifMenu : public Widget {
public:
    void draw() {
        cout << "MotifMenu\n";
    }
};

class WindowsButton : public Widget {
public:
    void draw() {
        cout << "WindowsButton\n";
    }
};

class WindowsMenu : public Widget {
public:
    void draw() {
        cout << "WindowsMenu\n";
    }
};

class MotifWidgetFactory : public WidgetFactory
{
public:
    Widget* create_menu() const
    {
        return new MotifMenu();
    }

    Widget* create_button() const
    {
        return new MotifButton();
    }
};

class WindowsWidgetFactory : public WidgetFactory
{
public:
    Widget* create_menu() const
    {
        return new WindowsMenu();
    }

    Widget* create_button() const
    {
        return new WindowsButton();
    }
};

class Window
{
    std::vector<Widget*> widgets;
public:
    void display() const
    {
        std::cout << "######################\n";
        for_each(widgets.begin(), widgets.end(), std::mem_fun(&Widget::draw));
        std::cout << "######################\n\n";
    }

    void add_widget(Widget* widget)
    {
        widgets.push_back(widget);
    }

    virtual ~Window()
    {
        std::vector<Widget*>::iterator it = widgets.begin();
        for(; it != widgets.end(); ++it)
            delete *it;
    }
};

class WindowOne : public Window
{
public:
    WindowOne(WidgetFactory& factory)
    {

        add_widget(factory.create_button());
        add_widget(factory.create_menu());
    }
};

class WindowTwo : public Window
{
public:
    WindowTwo(WidgetFactory& factory)
    {
        add_widget(factory.create_menu());
        add_widget(factory.create_button());
        add_widget(factory.create_button());
    }
};


int main(void)
{
#ifdef WINDOWS
    WindowsWidgetFactory widget_factory;
#else
    MotifWidgetFactory widget_factory;
#endif


    WindowOne w1(widget_factory);
    w1.display();

    WindowTwo w2(widget_factory);
    w2.display();
}
