#include "raport_builder.hpp"

using namespace std;

RaportBuilder& HtmlRaportBuilder::add_header( const std::string& header_text )
{
	doc_.clear();

	doc_.append("<h1>" + header_text + "</h1>\n");

	return *this;
}

RaportBuilder& HtmlRaportBuilder::begin_data()
{
	doc_.append("<table>\n");

	return *this;
}

RaportBuilder& HtmlRaportBuilder::add_row( const DataRow& data_row )
{
	doc_.append("  <tr>\n");
	for(DataRow::const_iterator item = data_row.begin(); item != data_row.end(); ++item)
	{
		doc_.append("    <td>" + (*item) + "</td>\n");
	}
	doc_.append("  </tr>\n");

	return *this;
}

RaportBuilder& HtmlRaportBuilder::end_data()
{
	doc_.append("<table>\n");

	return *this;
}

RaportBuilder& HtmlRaportBuilder::add_footer( const std::string& footer )
{
	doc_.append("<div class=footer>" + footer + "</div>\n");

	return *this;
}

HtmlDocument HtmlRaportBuilder::get_report()
{
	return doc_;
}


RaportBuilder& CsvRaportBuilder::add_header( const std::string& header_text )
{
	doc_.push_back("# " + header_text);

	return *this;
}

RaportBuilder& CsvRaportBuilder::begin_data()
{
	doc_.push_back("\n");

	return *this;
}

RaportBuilder& CsvRaportBuilder::add_row( const DataRow& data_row )
{

	string csv_row;
	for(DataRow::const_iterator item = data_row.begin(); item != data_row.end(); ++item)
	{
		csv_row.append((*item) + ";");
	}
	doc_.push_back(csv_row);

	return *this;
}

RaportBuilder& CsvRaportBuilder::end_data()
{
	doc_.push_back("\n");

	return *this;
}

RaportBuilder& CsvRaportBuilder::add_footer( const std::string& footer )
{
	doc_.push_back("# Summary: " + footer);

	return *this;
}

CsvDocument CsvRaportBuilder::get_report()
{
	return doc_;
}
