#ifndef EMPLOYEE_HPP_
#define EMPLOYEE_HPP_

#include <iostream>
#include <string>

class Employee
{
protected:
	std::string name_;
public:
	Employee(const std::string& name);

	virtual void description() const = 0;
    virtual std::string id() const = 0;

	virtual ~Employee();
};

class Salary : public Employee
{
public:
	Salary(const std::string& name);

    virtual std::string id() const
    {
        return Salary::ID;
    }

	virtual void description() const;

    static std::string ID;
};

class Hourly : public Employee
{
public:
	Hourly(const std::string& name);

	virtual void description() const;

    virtual std::string id() const
    {
        return Hourly::ID;
    }

    static std::string ID;
};

class Temp : public Employee
{
public:
	Temp(const std::string& name);

    virtual std::string id() const
    {
        return Temp::ID;
    }

	virtual void description() const;

    static std::string ID;
};

#endif /* EMPLOYEE_HPP_ */
