#include "employee.hpp"

std::string Salary::ID = "Salary";
std::string Hourly::ID = "Hourly";
std::string Temp::ID = "Temp";

Employee::Employee(const std::string& name) : name_(name)
{
}

Employee::~Employee()
{
}

Salary::Salary(const std::string& name) : Employee(name)
{
}

void Salary::description() const
{
	std::cout << "Salaried Employee: " << name_ << std::endl;
}

Hourly::Hourly(const std::string& name) : Employee(name)
{
}

void Hourly::description() const
{
	std::cout << "Hourly Employee: " << name_ << std::endl;
}

Temp::Temp(const std::string& name) : Employee(name)
{
}

void Temp::description() const
{
	std::cout << "Temporary Employee: " << name_ << std::endl;
}
