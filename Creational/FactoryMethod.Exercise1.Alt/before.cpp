#include <vector>
#include <map>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

#include "employee.hpp"
#include "hrinfo.hpp"

//HRInfo* gen_info(const Employee& e)
//{
//    if (const Salary* s = dynamic_cast<const Salary*>(&e))
//        return new StdInfo(s);
//    if (const Hourly* h = dynamic_cast<const Hourly*>(&e))
//        return new StdInfo(h);
//    if (const Temp* t = dynamic_cast<const Temp*>(&e))
//        return new TempInfo(t);
//    else
//        return 0;
//}

class HRInfoCreator
{
public:
    virtual HRInfo* create_hrinfo(Employee* emp) = 0;
    virtual ~HRInfoCreator()
    {}
};

class StdInfoCreator : public HRInfoCreator
{
public:
    HRInfo* create_hrinfo(Employee* emp)
    {
        return new StdInfo(emp);
    }
};

class TempInfoCreator : public HRInfoCreator
{
public:
    HRInfo* create_hrinfo(Employee* emp)
    {
        return new TempInfo(emp);
    }
};

class HRInfoFactory
{
    std::map<std::string, boost::shared_ptr<HRInfoCreator> > creators_;
public:
    void register_creator(const std::string& id, boost::shared_ptr<HRInfoCreator> creator)
    {
        creators_.insert(std::make_pair(id, creator));
    }

    boost::shared_ptr<HRInfo> create(Employee* emp)
    {
        std::string id = emp->id();

        return boost::shared_ptr<HRInfo>(creators_[id]->create_hrinfo(emp));
    }
};

int main()
{
	using namespace std;

    // bootstrap application
    HRInfoFactory factory;
    factory.register_creator(Salary::ID, boost::shared_ptr<HRInfoCreator>(new StdInfoCreator));
    factory.register_creator(Hourly::ID, boost::shared_ptr<HRInfoCreator>(new StdInfoCreator));
    factory.register_creator(Temp::ID, boost::shared_ptr<HRInfoCreator>(new TempInfoCreator));

	vector<Employee*> emps;
	emps.push_back(new Salary("Jan Kowalski"));
	emps.push_back(new Hourly("Adam Nowak"));
	emps.push_back(new Temp("Anna Nowakowska"));

	cout << "HR Report:\n---------------\n";
	// generowanie obiektów typu HRInfo
	for(size_t i = 0; i < emps.size(); ++i)
	{
        boost::shared_ptr<HRInfo> hri = factory.create(emps[i]);
		hri->info();
		cout << endl;
	} // wyciek pamięci

	// sprzątanie
	for(size_t i = 0; i < emps.size(); ++i)
		delete emps[i];
}
