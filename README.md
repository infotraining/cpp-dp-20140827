# README #

# ustawienie proxy

    Najwygodniej jest dodac wpis w pliku .profile

    export http_proxy=http://10.144.1.10:8080
    export https_proxy=https://10.144.1.10:8080


# test polaczenia

    ping 8.8.8.8 # przy zalozeniu, ze ICMP nie jest blokowane
    curl -v http://www.example.com/


# git

    git-cheat-sheet
    http://www.cheat-sheets.org/saved-copy/git-cheat-sheet.pdf

    git status  # status lokalnego repozytorium
    git pull    # pobranie plikow z repozytorium
    git stash   # przeniesienie modyfikacji do schowka